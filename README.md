##some requets 
`curl -v GET -H "Accept: application/json" http://localhost:8080/test/testme`

`curl -v GET -H 'Content-Type: application/x-www-form-urlencoded' -H "Accept: application/json" http://localhost:8083/xyz/emo`

### create client credentials access token
`curl -v -H "Authorization: Basic Y2U2MTk4MjQtOGMxMS00MDdkLWJmMmQtZTE0ZTdmYzg2ZDBmOjgxMWFkOWEwLWVhYWEtNDZkNS1iODBkLTYxNTg1NWZhMDUyNg==" POST -H "Content-Type: application/x-www-form-urlencoded" -H "Accept: application/json" -k http://localhost:8080/oauth/token -d "grant_type=client_credentials&scope=write+delete+read"`

### get access token

`curl -i -v POST -H 'Content-Type: application/x-www-form-urlencoded' -k http://localhost:8082/oauth/token -H 'Authorization: Basic Y2xpZW50OnNlY3JldA==' -d 'grant_type=password&client_id=client&username=emoleumassi&password=today&scope=write'`

### create new user
`curl -i -v POST -H "Authorization: Bearer e916938d-ad8c-4685-9c7a-16f44b0fc055" -H 'Content-Type: application/json' -d '{"role":"ADMIN", "username":"test", "email":"test@gmail.com", "password":"test"}' -k http://localhost:8083/user/create`