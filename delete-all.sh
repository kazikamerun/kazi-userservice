#! /bin/bash

echo "delete kazi-userservice-service"
kubectl delete service kazi-userservice-service

echo "\ndelete kazi-userservice-credentials"
kubectl delete secret kazi-userservice-credentials

echo "\ndelete kazi-userservice-configmap"
kubectl delete configmap kazi-userservice-config

echo "\ndelete kazi-userservice-deployment"
kubectl delete -n default deployment kazi-userservice-deployment

#echo "\nstop kazi-userservice-k8s docker container"
#docker stop $(docker ps -a -q --filter "ancestor=kazi-userservice-k8s")

#echo "\nremove kazi-userservice-k8s docker container"
#docker rm $(docker ps -a -q --filter "ancestor=kazi-userservice-k8s")

echo "\nremove kazi-userservice-k8s docker image"
docker rmi kazi-userservice-k8s
##docker ps -q --filter ancestor="kazi-userservice-k8s" | xargs -r docker stop