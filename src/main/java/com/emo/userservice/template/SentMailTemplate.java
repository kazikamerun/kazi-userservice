package com.emo.userservice.template;

import com.emo.userservice.bootstrap.KaziConfiguration;
import com.kazi.api.model.MailModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SentMailTemplate {

    @Autowired
    private KaziConfiguration kaziConfiguration;

    public void sent(final MailModel mailModel) {
        final String uri = kaziConfiguration.getMailUrl();
        final RestTemplate restTemplate = new RestTemplate();
        final String result = restTemplate.postForObject(uri, mailModel, String.class);

        System.out.println(result);
    }
}
