package com.emo.userservice.beans;

import com.kazi.api.service.RegistrationService;
import com.kazi.api.service.UserService;
import com.kazi.service.impl.RegistrationServiceImpl;
import com.kazi.service.impl.UserServiceImpl;
import com.kazi.service.validator.UserValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserServiceConfig {

    @Bean
    public UserService userServiceImpl() {
        return new UserServiceImpl();
    }

    @Bean
    public UserValidator userValidator(){
        return new UserValidator();
    }

    @Bean
    public RegistrationService registrationService(){
        return new RegistrationServiceImpl();
    }

}
