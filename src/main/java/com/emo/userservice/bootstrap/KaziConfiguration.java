package com.emo.userservice.bootstrap;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
//@ConfigurationProperties("kazi")
public class KaziConfiguration {

    @Value(value = "${kazi.mail.url}")
    private String mailUrl;
}
