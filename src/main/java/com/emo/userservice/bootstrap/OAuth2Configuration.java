package com.emo.userservice.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
@ConfigurationProperties("oauth.server.security")
public class OAuth2Configuration {

    @Setter
    @Getter
    public static class Password {
        private String id;
        private String secret;
        private String authorization;
    }

    @Setter
    @Getter
    public static class Credentials {
        private String id;
        private String secret;
        private String authorization;
    }

    private Password password;
    private Credentials credentials;
    private String tokenEndpoint;
}