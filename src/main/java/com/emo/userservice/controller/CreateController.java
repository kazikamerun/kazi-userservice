package com.emo.userservice.controller;

import com.emo.userservice.template.SentMailTemplate;
import com.kazi.api.details.UserDetails;
import com.kazi.api.model.MailModel;
import com.kazi.api.service.RegistrationService;
import com.kazi.api.service.UserService;
import com.kazi.service.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user", produces = "application/json")
public class CreateController {

    @Autowired
    private UserService userServiceImpl;
    @Autowired
    private UserValidator userValidator;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private SentMailTemplate mailTemplate;

    @PostMapping(value = "/create", consumes = "application/json")
    public ResponseEntity<String> createUser(@RequestBody @Validated UserDetails newUser, final BindingResult bindingResult) {

        userValidator.validate(newUser, bindingResult);
        final MailModel mailModel = userServiceImpl.create(newUser);
        mailTemplate.sent(mailModel);
        return new ResponseEntity<>("New user: " + newUser.toString() + " saved", HttpStatus.OK);
    }

    @PostMapping(value = "/validate", consumes = "application/json")
    public ResponseEntity<String> validate(@RequestBody @Validated String url) {

        registrationService.validateUrlToken(url);
        return new ResponseEntity<>("url " + url + " valid", HttpStatus.OK);
    }
}
