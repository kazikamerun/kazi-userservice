package com.emo.userservice.controller;

import com.kazi.api.details.UserDetails;
import com.kazi.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/xyz", produces = "application/json")
public class EmoController {

    @Autowired
    private UserService userServiceImpl;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ResponseEntity<UserDetails> getUser() {
        return new ResponseEntity<>(userServiceImpl.findByUsername("emoleumassi"), HttpStatus.OK);
    }

    @RequestMapping(value = "/emo", method = RequestMethod.GET)
    public ResponseEntity<String> testMethod() {
        return new ResponseEntity<>("Emo Leumassi hein", HttpStatus.OK);
    }


}
